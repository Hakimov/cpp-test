# Задача 2

### Есть класс CodeGenerator, который умеет генерить код на разных языках.
### Предложите рефакторинг с учетом, что количество языков будет расширяться

```cpp
class CodeGenerator
{
public:
    enum Lang {JAVA, C_PLUS_PLUS, PHP};
    CodeGenerator(Lang language) { _language=language; }
    std::string generateCode()
    {
        switch(_language) {
        case JAVA:        //return generated java code
        case C_PLUS_PLUS: //return generated C++ code
        case PHP:         //return generated PHP code
        }
        throw new std::logic_error("Bad language");
    }
    std::string someCodeRelatedThing() // used in generateCode()
    {
        switch(_language) {
        case JAVA:        //return generated java-related stuff
        case C_PLUS_PLUS: //return generated C++-related stuff
        case PHP:         //return generated PHP-related stuff
        }
        throw new std::logic_error("Bad language");
    }
    
private:
    Lang _language;
}
```

##### Решение
Во первых, необходимо отказаться от перчислений и следовать принципу "разделяй и властвуй".
То есть, для каждого генератора языка создать свой класс, который будет наследоватся от общего класса **Language** в котором заданы виртуальные функции **"someCodeRelatedThing"** и **"generateCode"**.
Для каждого генератора эти функции будут определены по своему.
При расширении количества языков необходимо будет просто создать для него новый класс, наследуемый от **Language** и переопределить соответствующие функции.
[Мой рефакторинг](t2)