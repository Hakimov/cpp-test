---
# Задача 3
### Все ли хорошо в этом коде?

**Файл legacy.c**
```cpp
int values[3];
```

**Файл modern.cpp**
```cpp
#define LEGACY_SIZE 3
extern int *values;

class MyBlah {...};

class Adapter
{
public:
    Adapter()
    {
        for (int i = 0; i < LEGACY_SIZE; ++i)
            map_[values[i]] = new MyBlah (values[i]);
    }
private:
    std::map<int, MyBlah *> map_;
};
```
***
#### Найденные проблемы
**1. Отсутвует деструктор**

Что приведет к утечке памяти.

**2. В цикле вместо *int* лучше использовать size_t**
```cpp
for (size_t i = 0; i < LEGACY_SIZE; ++i)
```
**3. В С++ вместо дефайнов лучше использовать константные переменные**

Вместо
```cpp
#define LEGACY_SIZE 3
```
Использовать
```cpp
const size_t LEGACY_SIZE 3
```
**4. Задать размер массива при объявлении**
```cpp
extern int *values;
```
Заменить на
```cpp
extern int values[LEGACY_SIZE];
```
И вообще, лучше не использовать глобальные переменные и массивы.
Лучше задавать для них методы **set** и **get** при использовании в разных файлах.

---
