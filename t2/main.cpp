#include <iostream>
#include "codegeneatorjava.h"
#include "codegeneratorcpp.h"
#include "codegeneratorphp.h"

using namespace std;

int main()
{
    CodeGeneatorJava cgJAVA;
    cout << cgJAVA.generateCode() << endl;

    CodeGeneratorCPP cgCPP;
    cout << cgCPP.generateCode() << endl;

    CodeGeneratorPHP cgPHP;
    cout << cgPHP.generateCode() << endl;



    return 0;
}
