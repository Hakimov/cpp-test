#ifndef CODEGENEATORJAVA_H
#define CODEGENEATORJAVA_H

#include "language.h"

class CodeGeneatorJava : public Language
{
public:
    CodeGeneatorJava();
    ~CodeGeneatorJava();

    std::string generateCode() override;
    std::string someCodeRelatedThing() override;
};

#endif // CODEGENEATORJAVA_H
