#ifndef CODEGENERATORPHP_H
#define CODEGENERATORPHP_H

#include "language.h"

class CodeGeneratorPHP : public Language
{
public:
    CodeGeneratorPHP();
    ~CodeGeneratorPHP();

    std::string generateCode() override;
    std::string someCodeRelatedThing() override;
};

#endif // CODEGENERATORPHP_H
