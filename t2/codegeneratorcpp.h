#ifndef CODEGENERATORCPP_H
#define CODEGENERATORCPP_H

#include "language.h"

class CodeGeneratorCPP : public Language
{
public:
    CodeGeneratorCPP();
    ~CodeGeneratorCPP();

    std::string generateCode() override;
    std::string someCodeRelatedThing() override;
};

#endif // CODEGENERATORCPP_H
