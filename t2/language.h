#ifndef LANGUAGE_H
#define LANGUAGE_H

#include <string>
#include <stdexcept>

class Language
{
public:
    ~Language();
    virtual std::string generateCode() = 0;
    virtual std::string someCodeRelatedThing() = 0;
};

#endif // LANGUAGE_H
