TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    language.cpp \
    codegeneatorjava.cpp \
    codegeneratorphp.cpp \
    codegeneratorcpp.cpp

HEADERS += \
    language.h \
    codegeneatorjava.h \
    codegeneratorphp.h \
    codegeneratorcpp.h
