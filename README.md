# PROSOFT SYSTEM TEST

[Задача 1](task1.md)

[Задача 2](task2.md)

[Задача 3](task3.md)

[Задача 4](task4.md)

**Реализация [линейного поиска](https://bitbucket.org/Hakimov/linear-search/src) на Си**

**Реализация [связного списка](https://bitbucket.org/Hakimov/single_linked_list/src) для хранения любых типов данных** (была сделана в качестве первого домашнего задания в *школе разработчиков прософт*).