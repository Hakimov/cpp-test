---
# Задача 4
### Что не так в этом коде? Перечислите, какие недостатки вы видите. Предложите свой вариант рефакторинга.
```cpp
#include <stdio.h>

class Feature
{
public:
    enum FeatureType {eUnknown, eCircle, eTriangle, eSquare};
    
    Feature() : type(eUnknown), points(0) {    }
    
    ~Feature()
    {
        if (points)
            delete points;
    }
    
    bool isValid() 
    {
        return type != eUnknown;
    }
    
    bool read(FILE* file)
    {
        if (fread(&type, sizeof(FeatureType), 1, file) != sizeof(FeatureType))
            return false;
        short n = 0;
        switch (type) 
        {
        case eCircle: n = 3; break;
        case eTriangle:    n = 6; break;
        case eSquare: n = 8; break;
        default: type = eUnknown; return false;
        }
        points = new double[n];
        if (!points)
            return false;
        return fread(&points, sizeof(double), n, file) == n*sizeof(double);
    }
    void draw()
    {
        switch (type)
        {
        case eCircle: drawCircle(points[0], points[1], points[2]); break;
        case eTriangle:    drawPoligon(points, 6); break;
        case eSquare: drawPoligon(points, 8); break;
        }
    }
    
protected:
    void drawCircle(double centerX, double centerY, double radius);
    void drawPoligon(double* points, int size);
    
    double* points;
    FeatureType type;
};

int main(int argc, char* argv[])
{
    Feature feature;
    FILE* file = fopen("features.dat", "r");
    feature.read(file);
    if (!feature.isValid())
        return 1;
    return 0;
}
```
----
##### Найденные недостатки:
* нет функции **fclose( file )** для закрытия файла;
* в деструкторе вместо **delete** нужно использовать **delete[]**;
* в *main* необходимо выполнить проверку на открытие файла;
* вместо Сишного *fopen*, лучше использовать *ifstream*;
* методы, которые не должны изменять члены класса, лучше сделать *const*;
* в cтрочке **return fread(&points, sizeof(double), n, file) == n*sizeof(double);** передается адрес *&points*, хотя *points* является указателем.
#####
----
### Мой вариант рефакторинга
[Здесь](t4) папка с проектом, где все классы упорядочены по отдельным файлам.

Проверку с реальным файлом *"feauture.dat"* не производил. Думаю, для Вас важно построение архитектуры приложения, а не конкретная ее реализация.

Преимуществом такого рефакторинга является модульность проекта. Каждый элемент (круг, треугольник, квадрат) наследуется от абстрактного класса **Figure**, который в свою очередь, является интерфейсом для взаимодействия через класс **FigMake**. Для каждого элемента переопределяется свой метод *draw* и *read*. При расширении класса необходимо создать новый класс, наследуясь от **Figure**, определить методы *read* и *draw*, и добавить еще один **case** в **FigMake**. 

*"Пользователю"* необходимо работать только с классом **FigMake**:

* открыть файл;
* создать объект класса **FigMake**, передав в его конструктор файл;
* вызвать метод *draw* для отрисовки;
* закрыть файл.

---
```cpp
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <assert.h>

using namespace std;

class Figure {
public:
    virtual ~Figure() {}
    virtual void read(std::istream& file) = 0;
    virtual void draw() const = 0;
protected:
    std::vector<double> points;
};


class Circle : public Figure {
public:
    Circle(){}
    void read(std::istream& file) override {
        points.resize(point_numb);
        file.read(reinterpret_cast<char*>(&points), sizeof(double) * point_numb);
        std::cout << "circle read" << std::endl;
    }
    void draw() const override {
        assert(points.size() == point_numb);
        std::cout << "circle draw" << std::endl;
        /* Реализация метода для отрисовки с использованием
         * point_numb и points
         */
    }
    ~Circle(){}
private:
    const size_t point_numb = 3;
};

class Triangle : public Figure {
public:
    Triangle(){}
    void read(std::istream& file) override {
        points.resize(point_numb);
        file.read(reinterpret_cast<char*>(&points), sizeof(double) * point_numb);
        std::cout << "triangle read" << std::endl;
    }
    void draw() const override {
        assert(points.size() == point_numb);
        std::cout << "triangle draw" << std::endl;
        /* Реализация метода для отрисовки с использованием
         * point_numb и points
         */
    }
    ~Triangle(){}
private:
    const size_t point_numb = 6;
};

class Square : public Figure {
public:
    Square(){}
    void read(std::istream& file) override {
        points.resize(point_numb);
        file.read(reinterpret_cast<char*>(&points), sizeof(double) * point_numb);
        std::cout << "square read" << std::endl;
    }
    void draw() const override {
        assert(points.size() == point_numb);
        std::cout << "square draw" << std::endl;
        /* Реализация метода для отрисовки с использованием
         * point_numb и points
         */
    }
    ~Square(){}
private:
    const size_t point_numb = 8;
};

enum class FigureType{eUnknown, eCircle, eTriangle, eSquare};
class FigMaker {
public:
    FigMaker(std::istream& file) {
        FigureType fig_type;
        file.read(reinterpret_cast<char*>(&fig_type), sizeof (FigureType));
        switch (fig_type) {
        case FigureType::eCircle:
            figure = new Circle();
            break;
        case FigureType::eSquare:
            figure = new Square();
            break;
        case FigureType::eTriangle:
            figure = new Triangle();
            break;
        default:
            throw std::string("Figure type error");
            break;
        }
        figure->read(file);
    }

    void draw() {
        figure->draw();
    }

private:
    Figure *figure;
};

int main()
{
    try {
        std::ifstream file("features.dat", std::fstream::in | std::fstream::binary);
        file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        FigMaker fm(file);
        fm.draw();
        file.close();
    }
    catch (std::ifstream::failure& is)
    {
        std::cout << "ifstream exception:" << is.code() << std::endl;
    }
    catch (...) {
        std::cout << "unexpected exception!" << std::endl;
    }
    return 0;
}
```
----