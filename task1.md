# Задача 1
### Перечислите все проблемы, которые вы видите в данном коде:
```cpp
class Foo
{
public:
    Foo(int j) { i=new int[j]; }
    ~Foo() { delete i; }
private:
    int* i;
};

class Bar: Foo
{
public:
    Bar(int j) { i=new char[j]; }
    ~Bar() { delete i; }
private:
    char* i;
};

void main()
{
    Foo* f=new Foo(100);
    Foo* b=new Bar(200);
    *f=*b;
    delete f;
    delete b;
}
```

## Решение

##### Ошибка №1
Вместо:
```cpp 
void main()
```
Должно быть
```cpp
int main()
```
а так же в теле **main** отсутвует 
```cpp
return 0;
```
##### Ошибка №2
Следующая строка не сработает
```cpp
Foo* b = new Bar(200);
```
Поскольку, либо необходимо задать классу **Foo** конструктор по умолчанию
```cpp
class Foo {
public:
    Foo() {} // конструктор по умолчанию
```
либо вызывать конструктор явно в **Bar**
```cpp
Bar(int j) : Foo(j) { i=new char[j]; }
```
##### Ошибка №3
При удалении объекта **b** произойдет ошибка, из-за того, что объекту **f** был присвоен **b**:
```cpp
*f = *b;
```
соответственно, при вызове
```cpp
delete b;
```
произойдет обращение к несуществующему указателю.



