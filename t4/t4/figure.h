#ifndef FIGURE_H
#define FIGURE_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <assert.h>

class Figure {
public:
    virtual ~Figure();
    virtual void read(std::istream& file) = 0;
    virtual void draw() const = 0;
protected:
    std::vector<double> points;
};

#endif // FIGURE_H
