#include <iostream>
#include "figmaker.h"

using namespace std;

int main()
{
    try {
        std::ifstream file("features.dat", std::fstream::in | std::fstream::binary);
        file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        FigMaker fm(file);
        fm.draw();
        file.close();
    }
    catch (std::ifstream::failure& is)
    {
        std::cout << "ifstream exception:" << is.code() << std::endl;
    }
    catch (...) {
        std::cout << "unexpected exception!" << std::endl;
    }
    return 0;
}
