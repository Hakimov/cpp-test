#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "figure.h"

class Triangle : public Figure {
public:
    Triangle();
    void read(std::istream& file) override;
    void draw() const override;
    ~Triangle();
private:
    const size_t point_numb = 6;
};

#endif // TRIANGLE_H
