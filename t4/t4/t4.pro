TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    circle.cpp \
    figmaker.cpp \
    figure.cpp \
    square.cpp \
    triangle.cpp

HEADERS += \
    circle.h \
    figmaker.h \
    figure.h \
    square.h \
    triangle.h
