#include "figmaker.h"

FigMaker::FigMaker(std::istream& file)
{
    FigureType fig_type;
    file.read(reinterpret_cast<char*>(&fig_type), sizeof (FigureType));
    switch (fig_type) {
    case FigureType::eCircle:
        figure = new Circle();
        break;
    case FigureType::eSquare:
        figure = new Square();
        break;
    case FigureType::eTriangle:
        figure = new Triangle();
        break;
    default:
        throw std::string("Figure type error");
        break;
    }
    figure->read(file);
}

void FigMaker::draw() {
    figure->draw();
}
