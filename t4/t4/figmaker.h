#ifndef FIGMAKER_H
#define FIGMAKER_H

#include "figure.h"
#include "circle.h"
#include "triangle.h"
#include "square.h"

enum class FigureType{ eUnknown, eCircle, eTriangle, eSquare };

class FigMaker {
public:
    FigMaker(std::istream& file);
    void draw();

private:
    Figure *figure;
};

#endif // FIGMAKER_H
