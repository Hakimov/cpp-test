#ifndef CIRCLE_H
#define CIRCLE_H

#include "figure.h"

class Circle : public Figure {
public:
    Circle();
    void read(std::istream& file) override;
    void draw() const override;
    ~Circle();
private:
    const size_t point_numb = 3;
};


#endif // CIRCLE_H
