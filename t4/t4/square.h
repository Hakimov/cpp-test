#ifndef SQUARE_H
#define SQUARE_H

#include "figure.h"

class Square : public Figure {
public:
    Square();
    void read(std::istream& file) override;
    void draw() const override;
    ~Square();
private:
    const size_t point_numb = 8;
};

#endif // SQUARE_H
